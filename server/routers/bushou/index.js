const router = require("express").Router();

router.get("/radicals", (req, res) =>
  res.send(require("./radicals/success.json"))
);

router.get("/episodes", (req, res) =>
  res.send(require("./episodes/success.json"))
);

router.get("/tokenize", (req, res) =>
  res.send(require("./tokenize/success.json"))
);

router.get("/images/:name", (req, res) => {
  res.sendFile(`${__dirname}/images/${encodeURIComponent(req.params.name)}`);
});

module.exports = router;
